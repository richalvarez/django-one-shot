from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

def todo_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list_list,
    }
    return render(request, "todos/list.html", context)


def show_todo_list(request, id):
    list_details = TodoList.objects.get(id=id)
    context = {
        "list_details": list_details
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

        context = {
            "form": form
        }
        return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    post = TodoList.objects.get(id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save()
            return redirect("todo_list_detail", id=post.id)

    else:
        form = TodoListForm(instance=post)
    context = {
        "form": form,
        "post": post,
    }
    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/itemcreate.html", context)

def update_todo_item(request, id):
    post = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save()
            return redirect("todo_list_detail", id=post.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
        "post": post,
    }
    return render(request, "todos/itemupdate.html", context)
